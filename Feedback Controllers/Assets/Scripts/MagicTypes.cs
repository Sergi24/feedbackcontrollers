﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MagicTypes
{
    Fire, Water, Life, Light, Shadows, Normal
}
