﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[SerializeField]
[Serializable]
public class PlayerStats : CharacterStats
{

    int experienceNeeded;
    int currentExperience;

    protected new void Start()
    {
        Debug.Log("Start playerStats");
        /*PlayerInfo playerInfo = GameManager.instance.Profile.playerInfo;
        nameCharacter = playerInfo.namePlayer;
        Level = playerInfo.Level;
        CurrentHealth = playerInfo.CurrentHealth;
        currentMana = playerInfo.currentMana;

        base.Start();
        EventManager.OnEquipmentChanged += OnEquipmentChanged;
        Init();
        EquipmentManager.instance.Init();*/
    }

    private new void Init()
    {
        currentExperience = 0;
        oldLevel = Level;

        currentExperience -= experienceNeeded;
        experienceNeeded = (int)(ConstantMultipliers.baseExperienceLevel * Level * ConstantMultipliers.extraExperienceNeededByLevelMultiplier);
    }

    private new void Update()
    {
        base.Update();

        if (currentExperience >= experienceNeeded)
        {
            UpdateLevel();
            LevelUPUpdateModifiers();
        }

        if (Input.GetKeyDown(KeyCode.T))
            Testing();
    }

    private void UpdateLevel()
    {
        int oldLevel = Level++;

        currentExperience -= experienceNeeded;
        experienceNeeded = (int)(ConstantMultipliers.baseExperienceLevel * Level * ConstantMultipliers.extraExperienceNeededByLevelMultiplier);
    }

    private void Testing()
    {
        currentExperience += 200;
    }

    protected void LevelUPUpdateModifiers()
    {

        Color c = Color.yellow;

        UpdateModifiers();

        CurrentHealth = maxHealth.GetValue();
        currentMana = maxMana.GetValue();
        oldLevel = Level;

        if (CurrentHealth > maxHealth.GetValue())
            CurrentHealth = maxHealth.GetValue();

        if (currentMana > maxMana.GetValue())
            currentMana = maxMana.GetValue();

    }

    private void UpdateModifiers()
    {
        maxHealth.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));
        maxMana.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));

        lifeRegeneration.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));
        manaRegeneration.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));

        damage.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.RemoveModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

        strengh.RemoveModifier(oldLevel * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.RemoveModifier(oldLevel * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.RemoveModifier(oldLevel * ConstantMultipliers.intelligenceExtraByLevelPoint);


        strengh.AddModifier(Level * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.AddModifier(Level * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.AddModifier(Level * ConstantMultipliers.intelligenceExtraByLevelPoint);

        maxHealth.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));
        maxMana.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));

        lifeRegeneration.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));
        manaRegeneration.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));

        damage.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.AddModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

    }

}
