﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformPlaneController : MonoBehaviour
{

    public GameObject planeCorpse, propeller;
    public float speed = 60f;
    public float turn = 90f;
    public float axisX, axisY, axisLateral;

    private void Update()
    {
        float turbo = 1;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            turbo = 2;
            propeller.GetComponent<RotateAtSpeed>().speed = 60;
        }
        else
            propeller.GetComponent<RotateAtSpeed>().speed = 30;

        // Avance fijo sin leer los ejes del teclado
        transform.Translate(Vector3.forward * speed * Time.deltaTime * turbo);

        // Rotaciones en los ejes
        axisX = Input.GetAxis("Horizontal");
        transform.Rotate(Vector3.up * axisX * turn * Time.deltaTime, Space.World);

        axisY = Input.GetAxis("Vertical");
        transform.Rotate(Vector3.right * axisY * turn * Time.deltaTime * 2);

        if (axisX > 0)
            planeCorpse.transform.Rotate(Vector3.forward * (-axisX * .7f) * turn * Time.deltaTime * 4);
        else
            planeCorpse.transform.Rotate(Vector3.forward * (-axisX * .7f) * turn * Time.deltaTime * 4);
        /*
        axisLateral = Input.GetAxis("Lateral");
        if (planeCorpse.transform.eulerAngles.z < 30 && planeCorpse.transform.eulerAngles.z > -30)
        {
            Debug.Log(planeCorpse.transform.eulerAngles.z);
            planeCorpse.transform.Rotate(Vector3.forward * axisLateral * turn * Time.deltaTime * 4);
        }*/

        if (axisLateral == 0)
        {
            planeCorpse.transform.rotation = Quaternion.Slerp(planeCorpse.transform.rotation, transform.rotation, 0.05f);
        }

    }

}
