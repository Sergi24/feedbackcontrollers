﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WeaponTrigger : MonoBehaviour
{
    public InputPlayerController inputPlayerController;
    public CharacterStats characterStats;

    private List<GameObject> enemyHits = new List<GameObject>();

    //public LayerMask layer;
    public List<string> objectiveTags;

    public bool canHit;

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        inputPlayerController = GetComponentInParent<InputPlayerController>();
        characterStats = GetComponentInParent<CharacterStats>();

    }

    private void OnTriggerEnter(Collider other)
    {
        // hit
    }

    private bool IsTagObjective(GameObject otherGO)
    {
        bool isTagObjective = false;
        foreach (string tag in objectiveTags)
        {
            if (tag == otherGO.tag)
            {
                isTagObjective = true;
                break;
            }
        }
        return isTagObjective;
    }

    private bool IsHitted(GameObject otherGO)
    {
        bool isEnemyHitted = false;
        foreach (GameObject enemyHit in enemyHits)
        {
            if (otherGO == enemyHit)
            {
                isEnemyHitted = true;
                break;
            }
        }
        return isEnemyHitted;
    }

    public void RestartEnemyHits()
    {
        enemyHits = new List<GameObject>();
    }
}
