﻿// Script: FootStepsController.cs
// Descripción: A partir de una base de datos del script FootStepsDatabase detecta materiales físicos  de los suelos no terrenos y la textura predominante en terrenos pisados y ejecuta un sonido.
// Autor: Sergi Martínez Balsells
// Fecha: 15/10/2018
// Licencia: Dominio público

using UnityEngine;

public class FootStepsController : MonoBehaviour
{
    public AudioSource stepsAudioSource;
    public FootStepsDatabase footStepsDatabase;

    private Animator animator;

    // Terrain
    int surfaceIndex = 0;

    Transform lFoot;
    Transform rFoot;

    public enum Foot
    {
        left, right
    };

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Use this for initialization
    void Start()
    {

        lFoot = animator.GetBoneTransform(HumanBodyBones.LeftFoot);
        rFoot = animator.GetBoneTransform(HumanBodyBones.RightFoot);

    }



    private void PlaySoundStepByPhysicMaterial(PhysicMaterial physicMaterial)
    {

        string s = physicMaterial.name.Replace(" (Instance)", "");

        foreach (PhysicMaterialSoundSteps pMat in footStepsDatabase.physicMaterials)
        {
            if (pMat.physicMaterial.name.Equals(s))
            {
                //Debug.Log("Material found: " + s);

                int n = UnityEngine.Random.Range(0, pMat.audioclips.Count);

                stepsAudioSource.clip = pMat.audioclips[n];
                stepsAudioSource.Play();
                continue;
            }

        }
    }

    private void PlaySoundStepByTerrainTexture(Texture texture)
    {
        foreach (TextureSoundSteps tSoundSteps in footStepsDatabase.terrainTextures)
        {
            if (tSoundSteps.texture.ToString().Equals(texture.ToString()))
            {
                //Debug.Log("Texture found: " + texture.ToString());

                int n = UnityEngine.Random.Range(0, tSoundSteps.audioclips.Count);

                stepsAudioSource.clip = tSoundSteps.audioclips[n];
                stepsAudioSource.Play();
                continue;
            }
        }
    }

    private void PlayStepSound(Foot foot)
    {
        // FEET RAYCASTING

        RaycastHit rayCastHit;

        Vector3 pos;

        if (foot == Foot.left)
        {
            pos = lFoot.TransformPoint(Vector3.zero);
        }
        else
        {
            pos = rFoot.TransformPoint(Vector3.zero);
        }


        if (Physics.Raycast(pos, -Vector3.up, out rayCastHit, 1))
        {

            //Debug.Log("GameObject detected: " + rayCastHit.collider.gameObject);

            Terrain terrain = rayCastHit.collider.gameObject.GetComponent<Terrain>();

            if (terrain != null)
            {
                Vector3 footPosition = rayCastHit.point;

                //Debug.Log("Terrain detected: " + terrain + ", Position: " + footPosition);

                // Get Texture and play it.
                surfaceIndex = GetMainTexture(terrain, footPosition);

                //Debug.Log("Index: " + surfaceIndex + ", Texture: " + terrain.terrainData.splatPrototypes[surfaceIndex].texture.name);

                PlaySoundStepByTerrainTexture(terrain.terrainData.splatPrototypes[surfaceIndex].texture);

                return;
            }

            Collider pMat = rayCastHit.collider.gameObject.GetComponent<Collider>();

            if (pMat != null)
            {
                //Debug.Log("Physic Material detected: " + pMat.material.name);

                PlaySoundStepByPhysicMaterial(pMat.material);

            }

        }
    }

    float[] GetTextureMix(TerrainData tData, Vector3 tPos, Vector3 footPosition)
    {
        // returns an array containing the relative mix of textures
        // on the main terrain at this world position.

        // The number of values in the array will equal the number
        // of textures added to the terrain.

        // calculate which splat map cell the worldPos falls within (ignoring y)
        int mapX = (int)(((footPosition.x - tPos.x) / tData.size.x) * tData.alphamapWidth);
        int mapZ = (int)(((footPosition.z - tPos.z) / tData.size.z) * tData.alphamapHeight);

        // get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
        float[,,] splatmapData = tData.GetAlphamaps(mapX, mapZ, 1, 1);

        // extract the 3D array data to a 1D array:
        float[] cellMix = new float[splatmapData.GetUpperBound(2) + 1];

        for (int i = 0; i < cellMix.Length; i++)
        {
            cellMix[i] = splatmapData[0, 0, i];
        }

        return cellMix;
    }


    int GetMainTexture(Terrain terrain, Vector3 footPosition)
    {
        TerrainData tData = terrain.terrainData;
        Vector3 tPos = terrain.transform.position;

        // returns the zero-based index of the most dominant texture
        // on the main terrain at this world position.
        float[] mix = GetTextureMix(tData, tPos, footPosition);

        float maxMix = 0;
        int maxIndex = 0;

        // loop through each mix value and find the maximum
        for (int i = 0; i < mix.Length; i++)
        {
            if (mix[i] > maxMix)
            {
                maxIndex = i;
                maxMix = mix[i];
            }
        }

        return maxIndex;
    }

}



