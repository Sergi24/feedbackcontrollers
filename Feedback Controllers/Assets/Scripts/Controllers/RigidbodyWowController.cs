﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyWowController : MyController
{

    private Rigidbody rb;
    private Animator animator;
    private float axisY, axisX, axisLateral;

    public float runSpeed = 20;
    public float walkSpeed = 7;
    public float speedBack = 5;
    private float lateralSpeed = 6;

    public float jumpImpulse = 10;
    private bool jump = false;

    public float turn = 125;
    [Range(0, 1)]
    public float turnControl = 1;
    [Range(0, 1)]
    public float turnAirControl = .5f;

    public bool isGrounded;

    public float temp = .1f;
    public LayerMask groundLayer;
    public float groundCheckDistance = .3f;

    private float forwardSpeed;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        // Get Input
        axisY = Input.GetAxis("Vertical");
        axisX = Input.GetAxis("Horizontal");
        axisLateral = Input.GetAxis("Lateral");
        isGrounded = Physics.CheckSphere(transform.position + Vector3.up * temp, groundCheckDistance, groundLayer, QueryTriggerInteraction.Ignore);

        // Salto
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            jump = true;

        // Attacks
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3))
        {
            isAttacking = true;
        }

        // Velocity forwardSpeed
        if (Input.GetKey(KeyCode.LeftShift))
        {
            forwardSpeed = runSpeed;
            isRunning = true;
        }
        else
        {
            forwardSpeed = walkSpeed;
            isRunning = false;
        }

        // Movimiento
        if (!isAttacking)
        {
            if (axisY > 0)
            {
                transform.Translate(Vector3.forward * forwardSpeed * axisY * Time.deltaTime);
            }
            else if (axisY < 0)
            {
                transform.Translate(Vector3.forward * speedBack * axisY * Time.deltaTime);
            }
        }

        // Rotación
        transform.Translate(Vector3.right * lateralSpeed * axisLateral * Time.deltaTime);

        // Movimiento lateral
        if (isGrounded)
            transform.Rotate(Vector3.up * turn * axisX * turnControl * Time.deltaTime);
        else
            transform.Rotate(Vector3.up * turn * axisX * turnAirControl * Time.deltaTime);

        UpdateAnimator();
    }

    private void FixedUpdate()
    {
        // Salto
        if (jump && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpImpulse, ForceMode.Impulse);
            jump = false;
        }
    }

    private void UpdateAnimator()
    {
        // Movimiento
        if (axisY >= 0)
            if (isRunning)
                animator.SetFloat("forward", axisY);
            else
                animator.SetFloat("forward", axisY / 2);
        else
            animator.SetFloat("forward", axisY / 2);

        // turn
        animator.SetFloat("turn", axisX * 2);

        // movimiento lateral
        animator.SetFloat("lateral", axisLateral / 2);

        // Salto/Caida
        animator.SetFloat("speedY", rb.velocity.y);
        animator.SetBool("isGrounded", isGrounded);

        // Ataques (no else xk podria haber muchos más ataques).
        if (isAttacking)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                animator.SetTrigger("Attack1");
            else if (Input.GetKeyDown(KeyCode.Alpha2))
                animator.SetTrigger("Attack2");
            else if (Input.GetKeyDown(KeyCode.Alpha3))
                animator.SetTrigger("Attack3");
        }

    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position + Vector3.up * temp, groundCheckDistance);
    }

}
