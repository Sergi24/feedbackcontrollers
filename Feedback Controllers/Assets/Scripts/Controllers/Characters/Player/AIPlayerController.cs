﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIPlayerController : MonoBehaviour
{
    public Transform target;

    public NavMeshAgent agent;
    public PlayerAnimationController character;

    public float agentSpeed;

    private void Start()
    {
        agent.updateRotation = false;
        agent.updatePosition = true;
    }

    private void Update()
    {
        Moving();
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
        agent.speed = agentSpeed;
    }

    public void Moving()
    {

        // Move
        if (target != null)
            agent.SetDestination(target.position);

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity);
        }
        else
            character.Move(Vector3.zero);

    }
}
