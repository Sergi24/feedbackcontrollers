using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class PlayerAnimationController : CharacterControllerStandardAssets
{

    public void TakeItem()
    {
        animator.SetTrigger("TakeItem");
    }

    public new void Attack()
    {
        if (animator.GetBool("Attack1") && animator.GetBool("Attack2") && animator.GetBool("Attack3"))
        {
            //Debug.Log("All attacks activated");
        }
        else if (animator.GetBool("Attack1") && animator.GetBool("Attack2"))
        {
            animator.SetBool("Attack3", true);
        }
        else if (animator.GetBool("Attack1"))
        {
            animator.SetBool("Attack2", true);
        }
        else
        {
            animator.SetBool("Attack1", true);
        }
    }

    public override bool IsAttacking()
    {
        return animator.GetBool("Attack1");
    }

    public void Armed()
    {
        if (animator.GetBool("Armed") == true)
        {
            animator.SetBool("Armed", false);
        }
        else
        {
            animator.SetBool("Armed", true);
        }
    }

}
