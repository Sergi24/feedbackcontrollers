using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Cameras;

[RequireComponent(typeof(PlayerAnimationController))]
public class InputPlayerController : MonoBehaviour
{
    private PlayerAnimationController playerAnimationController; // A reference to the ThirdPersonCharacter on the object
    private Transform cam;                  // A reference to the main camera in the scenes transform
    private Vector3 camForward;             // The current forward direction of the camera
    private Vector3 move;

    public CharacterStats characterStats;


    public bool canMoveCharacter = true;

    public bool canInteract = false;

    public bool isAttacking;

    public List<WeaponTrigger> weaponTriggers;
    public ShieldStats shieldStats;

    private List<GameObject> objectives;

    public bool canHit;

    private void Start()
    {
        cam = Camera.main.transform;


        // get the third person character ( this should never be null due to require component )
        playerAnimationController = GetComponent<PlayerAnimationController>();



        canHit = false;
    }

    public void Init()
    {
        weaponTriggers = new List<WeaponTrigger>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            // Open Inventory
        }


        if (canMoveCharacter)
        {
            // Interaction
            if (Input.GetKeyDown(KeyCode.E) && canInteract)
            {
                // Take item
            }
            // Attack
            if (Input.GetMouseButtonDown(0))
            {
                Attack();
            }
            // Armed & unarmed
            else if (Input.GetKeyDown(KeyCode.X))
            {
                playerAnimationController.Armed();
            }
        }
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        if (canMoveCharacter && !isAttacking)
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");

            // calculate move direction to pass to character
            if (cam != null)
            {
                // calculate camera relative direction to move:
                camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
                move = v * camForward + h * cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                move = v * Vector3.forward + h * Vector3.right;
            }

            // pass all parameters to the character control script
            playerAnimationController.Move(move);
        }
        else
            playerAnimationController.Move(Vector3.zero);
    }

    private void Attack()
    {
        /*if (playerAnimationController.IsAttacking() == false)
        {*/

        Collider[] hitColliders = Physics.OverlapSphere(transform.position + Vector3.up, 2);

        objectives = new List<GameObject>();

        foreach (Collider c in hitColliders)
        {
            CharacterStats cStats = c.GetComponent<CharacterStats>();

            if (cStats != null && cStats != GetComponent<PlayerStats>() && cStats.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                objectives.Add(cStats.gameObject);
            }

        }

        // Si no es null cogemos el m�s cercano.
        if (objectives.Count != 0)
        {
            float minDistance = 5;
            GameObject tempGO = objectives[0];

            foreach (GameObject gO in objectives)
            {
                float distance = Vector3.Distance(gO.transform.position, transform.position);
                if (minDistance > distance)
                    tempGO = gO;
            }

            Vector3 relativePos = tempGO.transform.position - transform.position;
            transform.rotation = Quaternion.LookRotation(relativePos, Vector3.up);

        }

        if (weaponTriggers.Count != 0)
        {
            weaponTriggers[0].RestartEnemyHits();
            weaponTriggers[0].canHit = true;
        }

        isAttacking = true;

        //StartCoroutine(IsAttacking(weaponTriggers[0]));

        playerAnimationController.Attack();
        //}
    }

    public void Hurt()
    {
        playerAnimationController.Hurt();
    }

    public void Blocked()
    {
        playerAnimationController.Blocked();
    }

    public void Die()
    {
        canMoveCharacter = false;
        playerAnimationController.Die();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + Vector3.up, 2);

    }

    public IEnumerator IsAttacking(WeaponTrigger weaponTrigger)
    {
        while (playerAnimationController.IsAttacking() == true)
        {

            yield return null;
        }
        weaponTrigger.canHit = false;
    }



}
