﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPersonajeRigidbody : MonoBehaviour
{

    private Rigidbody rb;
    public float Yaxis, Xaxis;
    public float force = 30;
    public float turn = 180;
    public float jumpImpulse = 5;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Yaxis = Input.GetAxis("vertical");
        Xaxis = Input.GetAxis("Horizontal");
        transform.Rotate(Vector3.up * turn * Xaxis * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        //rb.AddRelativeForce(Vector3.forward * force * Yaxis);

        float YSpeed = rb.velocity.y;
        rb.velocity = transform.forward * force * Yaxis;
        rb.velocity = new Vector3(rb.velocity.x, YSpeed, rb.velocity.z);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.forward * jumpImpulse, ForceMode.Impulse);
        }

    }
}
