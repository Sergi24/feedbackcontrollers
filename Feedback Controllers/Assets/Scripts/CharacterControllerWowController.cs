﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerWowController : MyController
{
    public float runSpeed = 20;
    public float walkSpeed = 7;
    public float backSpeed = 5;
    public float lateralSpeed = 6;

    public float jump = 0.6f;
    public float gravity = 2f;

    public float turnSpeed = 125;
    [Range(0, 1)]
    public float turnControl = 1;
    [Range(0, 1)]
    public float turnAirControl = .5f;

    private CharacterController cc;
    private Animator animator;
    private Vector3 movement;

    private float axisX;
    private float axisY;
    private float axisLateral;

    private float forwardSpeed;

    void Start()
    {
        cc = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // Entrada de ejes
        axisY = Input.GetAxis("Vertical");
        axisX = Input.GetAxis("Horizontal");
        axisLateral = Input.GetAxis("Lateral");

        // Attacks
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3))
        {
            isAttacking = true;
        }

        // Velocity forwardSpeed
        if (Input.GetKey(KeyCode.LeftShift))
        {
            forwardSpeed = runSpeed;
            isRunning = true;
        }
        else
        {
            forwardSpeed = walkSpeed;
            isRunning = false;
        }

        // Movimiento avance o retroceso.
        if (cc.isGrounded && !isAttacking)
        {
            if (axisY > 0)
                movement = transform.forward * axisY * forwardSpeed * Time.deltaTime;
            else
                movement = transform.forward * axisY * backSpeed * Time.deltaTime;

            // Movimiento lateral
            movement = movement + transform.right * axisLateral * lateralSpeed * Time.deltaTime;
        }
        else if (isAttacking)
            movement = new Vector3(0, movement.y, 0);

        // Giro
        if (cc.isGrounded)
            transform.Rotate(0, axisX * turnSpeed * turnControl * Time.deltaTime, 0);
        else
            transform.Rotate(0, axisX * turnSpeed * turnAirControl * Time.deltaTime, 0);

        if (Input.GetKeyDown(KeyCode.Space) && cc.isGrounded && !isAttacking)
        {
            // Salto
            movement.y = jump;
        }
        else
        {
            movement.y -= gravity * Time.deltaTime;
        }

        // Movimiento final
        cc.Move(movement);

        // Update the animator
        UpdateAnimator(movement);

    }

    private void UpdateAnimator(Vector3 movement)
    {
        // Movimiento
        if (axisY >= 0)
            if (isRunning)
                animator.SetFloat("forward", axisY);
            else
                animator.SetFloat("forward", axisY / 2);
        else
            animator.SetFloat("forward", axisY / 2);

        // turn
        animator.SetFloat("turn", axisX * 2);

        // movimiento lateral
        animator.SetFloat("lateral", axisLateral / 2);

        // Salto/Caida
        animator.SetFloat("speedY", movement.y);
        animator.SetBool("isGrounded", cc.isGrounded);

        // Ataques (no else xk podria haber muchos más ataques).
        if (isAttacking)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                animator.SetTrigger("Attack1");
            else if (Input.GetKeyDown(KeyCode.Alpha2))
                animator.SetTrigger("Attack2");
            else if (Input.GetKeyDown(KeyCode.Alpha3))
                animator.SetTrigger("Attack3");
        }

    }


}
