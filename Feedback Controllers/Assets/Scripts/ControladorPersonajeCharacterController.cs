﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPersonajeCharacterController : MonoBehaviour
{

    private CharacterController cc;

    public float axisX;
    public float axisY;
    public Vector3 movement;

    public float speed = 10;
    public float turn = 120;

    private void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    private void Update()
    {
        axisY = Input.GetAxis("Vertical");
        axisX = Input.GetAxis("Horizontal");
        movement = Vector3.forward * axisY * speed * Time.deltaTime;
        movement = movement + Vector3.right * axisX * speed * Time.deltaTime / 2;

        cc.Move(movement);

    }

}
