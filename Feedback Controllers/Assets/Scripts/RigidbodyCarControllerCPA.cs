﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixRigidbodyCarControllerCPA : MonoBehaviour
{

    private Rigidbody rb;
    public float force;
    public float turnForce;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        // Forward force
        rb.AddRelativeForce(Input.GetAxis("Vertical") * force * Vector3.forward);
    }

    private void Update()
    {
        // Turn
        transform.Rotate(Input.GetAxis("Horizontal") * turnForce * Vector3.up);
    }
}
