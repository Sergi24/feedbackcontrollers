﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorCharacterController : MonoBehaviour
{

    private CharacterController cc;

    public float speed = 6f;
    public float turn = 90f;
    public float jump = 8f;
    public float gravity = 20f;

    public Vector3 movement = Vector3.zero;
    public Vector3 previousMovement;

    // Use this for initialization
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        previousMovement = movement;
        movement = transform.forward * Input.GetAxis("Vertical") * speed;
        movement.y = previousMovement.y;

        // Solo respondemos a controles si está en el suelo
        if (cc.isGrounded)
        {
            // Jump
            if (Input.GetKey(KeyCode.Space))
            {
                movement.y = jump;
            }
        }
        else
        {
            // Podriamos hacer esto siempre aunque no esté en el aire
            movement.y -= gravity * Time.deltaTime;
        }

        // Turn
        transform.Rotate(Input.GetAxis("Horizontal") * Vector3.up * turn * Time.deltaTime);

        // Final move
        cc.Move(movement * Time.deltaTime);

    }
}
