﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicalRigidbodyPlaneControllerCPA : MonoBehaviour
{

    private Rigidbody rb;
    public float force;
    public float turnForce;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        rb.AddRelativeForce(force * Vector3.forward);

        rb.AddRelativeTorque(Input.GetAxis("Vertical") * turnForce * Vector3.right);
        rb.AddRelativeTorque(Input.GetAxis("Horizontal") * turnForce * Vector3.forward);

        Debug.Log("Speed: " + rb.velocity + " = " + rb.velocity.magnitude + "m/s");

    }
}
