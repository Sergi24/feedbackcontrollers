﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack2StateMachineController : StateMachineBehaviour
{
    InputPlayerController inputPlayerController;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        inputPlayerController = animator.GetComponent<InputPlayerController>();

        animator.SetBool("IsAttacking", true);

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float currentPercentage = stateInfo.normalizedTime;
        //Debug.Log(currentPercentage);

        if (currentPercentage < .40 && currentPercentage > .35)
        {
            inputPlayerController.canHit = true;
            //Debug.Log("isattacking = true");
        }
        else if (currentPercentage < .70 && currentPercentage > .65)
        {
            inputPlayerController.canHit = false;
            //Debug.Log("isattacking = false");
        }

        // Next attack
        if (currentPercentage > .90)
        {
            animator.SetBool("NextAttack", true);
        }

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (inputPlayerController.weaponTriggers.Count > 0)
            inputPlayerController.weaponTriggers[0].RestartEnemyHits();
        animator.SetBool("IsAttacking", false);
        animator.SetBool("NextAttack", false);
    }
}
