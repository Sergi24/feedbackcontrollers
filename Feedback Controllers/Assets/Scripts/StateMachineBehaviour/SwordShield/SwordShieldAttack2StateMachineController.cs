﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordShieldAttack2StateMachineController : StateMachineBehaviour
{

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        float currentPercentage = stateInfo.normalizedTime;
        //Debug.Log(currentPercentage);


    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("IsAttacking", false);
    }
}
