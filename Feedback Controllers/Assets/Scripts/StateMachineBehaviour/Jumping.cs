﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : StateMachineBehaviour
{
    public float cont = 0;
    public float minimumTime = 1;

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        cont += Time.deltaTime;
        if (cont >= minimumTime)
            animator.SetBool("MinimumTimeEnded", true);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("MinimumTimeEnded", false);
    }

}
