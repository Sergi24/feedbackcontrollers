﻿using UnityEngine;
using System.Collections;

public class RearWheelDrive : MonoBehaviour
{

    private WheelCollider[] wheels;

    public float maxAngle = 30;
    public float maxTorque = 300;
    public int posSteelWheels = 0;

    private Rigidbody rb;
    private float oldRotation;

    [Range(0, 1)]
    [SerializeField]
    private float steerHelper; // 0 is raw physics , 1 the car will grip in the direction it is facing

    public float currentSpeed;
    public float maxSpeed = 120;

    [Range(0, 1)]
    [SerializeField]
    public float decelerationSpeed;
    private float maxHandBrakeTorque;

    // here we find all the WheelColliders down in the hierarchy
    public void Start()
    {
        wheels = GetComponentsInChildren<WheelCollider>();
        rb = GetComponent<Rigidbody>();

        maxHandBrakeTorque = float.MaxValue;
    }

    // this is a really simple approach to updating wheels
    // here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
    // this helps us to figure our which wheels are front ones and which are rear
    public void Update()
    {
        float angle = maxAngle * Input.GetAxis("Horizontal");
        float torque = maxTorque * Input.GetAxis("Vertical");
        float handbrake = Input.GetAxis("Jump");

        // Nos aseguramos que handbrake está entre 0 y 1
        handbrake = Mathf.Clamp(handbrake, 0, 1);

        foreach (WheelCollider wheel in wheels)
        {
            // a simple car where front wheels steer while rear ones drive
            if (wheel.transform.localPosition.z > posSteelWheels)
            {
                wheel.steerAngle = angle;
                if (torque < 0)
                {
                    wheel.brakeTorque = -torque * 10;
                }
                else
                {
                    wheel.brakeTorque = 0;
                }
            }

            if (wheel.transform.localPosition.z < posSteelWheels)
            {
                if (torque > 0)
                {
                    wheel.motorTorque = torque;
                    wheel.brakeTorque = 0;
                }
                else if (torque < 0)
                {
                    wheel.brakeTorque = -torque * 10;
                }
                else
                {
                    wheel.motorTorque = 0;
                }
            }

            // Si no está acelerando desacelera.
            if (torque == 0)
                wheel.brakeTorque = decelerationSpeed * maxHandBrakeTorque;

            // Si no está acelerando y va a una velocidad muy baja se para.
            if (torque == 0 && currentSpeed < 2f && currentSpeed > -2f)
            {
                rb.velocity = Vector3.zero;
                wheel.brakeTorque = Mathf.Infinity;
            }

            // Si el jugador está frenando, frenamos
            if (handbrake > 0f)
                wheel.brakeTorque = handbrake * maxHandBrakeTorque;
            else // Si no está frenando reducimos el frenado a 0.
                wheel.brakeTorque = 0;

            UpdateVisualWheel(wheel);

        }

        // Una ayudita para girar más cómodamente.
        SteerHelper();
        // Obtenemos la velocidad en km/h
        currentSpeed = rb.velocity.magnitude * 3.6f;

    }

    private void SteerHelper()
    {
        // Verificamos que todas las ruedas están tocando el suelo. En caso de no ser así anulamos la ayuda de giro.
        foreach (WheelCollider wheel in wheels)
        {
            WheelHit wheelhit;
            wheel.GetGroundHit(out wheelhit);
            if (wheelhit.normal == Vector3.zero)
                return;
        }

        // Miramos si el giro no pasa del límite y de ser así aplicamos la rotación pertinente.
        if (Mathf.Abs(oldRotation - transform.eulerAngles.y) < 10f)
        {
            float turnAdjust = (transform.eulerAngles.y - oldRotation) * steerHelper;
            Quaternion velRotation = Quaternion.AngleAxis(turnAdjust, Vector3.up);
            rb.velocity = velRotation * rb.velocity;
        }
        // Guardamos el angulo Y para el siguiente frame verificar si necesitamos o no la ayuda.
        oldRotation = transform.eulerAngles.y;
    }

    private void UpdateVisualWheel(WheelCollider wheel)
    {
        // update visual wheels if any

        Quaternion q;
        Vector3 p;
        wheel.GetWorldPose(out p, out q);

        // assume that the only child of the wheelcollider is the wheel shape
        Transform shapeTransform = wheel.transform.GetChild(0);
        shapeTransform.position = p;
        shapeTransform.rotation = q;

    }
}
