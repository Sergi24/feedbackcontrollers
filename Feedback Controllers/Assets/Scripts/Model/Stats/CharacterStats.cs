﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[SerializeField]
[Serializable]
public class CharacterStats : MonoBehaviour
{
    protected InputPlayerController inputPlayerController;

    public int idCharacter;

    public string nameCharacter;

    public int Level;

    public int CurrentHealth;
    public int currentMana;

    public Stat maxHealth;
    public Stat maxMana;

    public Stat strengh;
    public Stat agility;
    public Stat intelligence;

    public Stat damage;
    public Stat armor;

    public Stat lifeRegeneration;
    public Stat manaRegeneration;

    // Not implemented
    public MagicTypes typeDamage;

    public GameObject unarmedWeapon;
    public GameObject armedWeapon;

    public GameObject unarmedShield;
    public GameObject armedShield;

    private float timeCounter;

    public bool ignoreCharacter = false;
    public bool dead;
    public bool inCombat;
    public bool isAddedToCombatMusic = false;

    protected int oldLevel;
    public int experience = 100;

    public GameObject PickUpItemPrefab;

    private void Awake()
    {

    }

    protected void Start()
    {
        // Test

        inputPlayerController = GetComponent<InputPlayerController>();


        timeCounter = 0;
        Init();
    }

    public void Init()
    {

        maxHealth.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));
        maxMana.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));

        lifeRegeneration.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));
        manaRegeneration.RemoveModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));

        damage.RemoveModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.RemoveModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

        strengh.RemoveModifier(oldLevel * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.RemoveModifier(oldLevel * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.RemoveModifier(oldLevel * ConstantMultipliers.intelligenceExtraByLevelPoint);

        // Add modifiers

        strengh.AddModifier(Level * ConstantMultipliers.strenghExtraByLevelPoint);
        agility.AddModifier(Level * ConstantMultipliers.agilityExtraByLevelPoint);
        intelligence.AddModifier(Level * ConstantMultipliers.intelligenceExtraByLevelPoint);

        maxHealth.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.lifeExtraByStrenghPoint));

        lifeRegeneration.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.extraLifeRegenerationByStrenghPoint));

        damage.AddModifier((int)(strengh.GetValue() * ConstantMultipliers.damageExtraByStrenghPointn));
        armor.AddModifier((int)(agility.GetValue() * ConstantMultipliers.armorExtraByAgilityPoint));

        if (!GetType().Equals(typeof(PlayerStats)))
        {
            CurrentHealth = maxHealth.GetValue();
        }


        if (maxMana.baseValue != 0)
        {
            maxMana.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.manaExtraByIntelligencePoint));
            manaRegeneration.AddModifier((int)(intelligence.GetValue() * ConstantMultipliers.extraManaRegenerationByStrenghPoint));
            currentMana = maxMana.GetValue();
        }

    }

    protected void Update()
    {
        timeCounter += Time.deltaTime;
        if (timeCounter >= 1 && !dead)
        {
            if (CurrentHealth + lifeRegeneration.GetValue() > maxHealth.GetValue())
                CurrentHealth = maxHealth.GetValue();
            else
                CurrentHealth += lifeRegeneration.GetValue();

            if (currentMana + manaRegeneration.GetValue() > currentMana)
                currentMana = maxMana.GetValue();
            else
                currentMana += manaRegeneration.GetValue();

            timeCounter = 0;
        }
        PreventXZAxisRotation();
    }



    public void TakeDamage(CharacterStats characterStatsOrigin)
    {
        // Take Damage it call GetDamage if proceed
    }

    private void GetDamage(CharacterStats characterStatsOrigin)
    {
        // Get Damage
    }

    public virtual void Die(CharacterStats characterStatsKiller)
    {
        // Die
    }

    public void DropDroppableItems()
    {
    }

    public void StartCombat(Transform target)
    {

    }

    public void EndCombat(Transform target)
    {
    }

    private void PreventXZAxisRotation()
    {
        //transform.rotation = Quaternion.Euler(0, transform.rotation.y, 0);

        Quaternion q = transform.rotation;
        q.eulerAngles = new Vector3(0, q.eulerAngles.y, 0);
        transform.rotation = q;
    }

}
