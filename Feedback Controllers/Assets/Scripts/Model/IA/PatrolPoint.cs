﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PatrolPoint : MonoBehaviour
{

    public bool isCharacterComming;
    public float timeWaiting;

}
