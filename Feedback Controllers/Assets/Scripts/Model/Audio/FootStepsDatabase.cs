﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New FootStepsDatabase", menuName = "Database/Foot Steps Database")]
public class FootStepsDatabase : ScriptableObject
{

    public List<PhysicMaterialSoundSteps> physicMaterials;
    public List<TextureSoundSteps> terrainTextures;

}

[Serializable]
[SerializeField]
public class PhysicMaterialSoundSteps
{
    public PhysicMaterial physicMaterial;
    public List<AudioClip> audioclips;
}

[Serializable]
public class TextureSoundSteps
{
    public Texture texture;
    public List<AudioClip> audioclips;
}