﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Combat Sound Database", menuName = "Database/Combat Sound Database")]
public class CombatSoundsDatabase : ScriptableObject
{
    public List<AudioClip> swordAttackClips;
    public List<AudioClip> shieldBlockClips;
}
