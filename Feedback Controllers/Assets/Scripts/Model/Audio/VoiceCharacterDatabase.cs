﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Voice Character Database", menuName = "Database/Voices Character Database")]
public class VoiceCharacterDatabase : ScriptableObject
{
    public List<AudioClip> usualClips;
    public List<AudioClip> attackClips;
    public List<AudioClip> huntingClips;
    public List<AudioClip> hurtClips;
    public List<AudioClip> deadClips;
    public List<AudioClip> screamClips;

}