﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Music Ambience Database", menuName = "Database/Music Database")]
public class MusicDatabase : ScriptableObject
{

    public List<AudioClip> musicClips;
    public List<AudioClip> combatClips;


}

