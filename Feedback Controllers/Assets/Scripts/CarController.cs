﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    // Wheel colliders
    public WheelCollider wheelFL, wheelFR, wheelBL, wheelBR;

    // Wheel Models
    public GameObject fL, fR, bL, bR;

    // Top speed
    public float topSpeed = 250f;
    // Max torque to apply to wheels.
    public float maxTorque = 200f;

    public float maxSteerAngle = 45;

    public float currentSpeed;

    public float maxBrakeTorque = 2200;
    // Motor force
    public float motorForce = 50;

    // Forward axis
    private float forward;
    // Turn axis
    private float turn;
    // Brake axis
    private float brake;

    private Rigidbody rb;

    public bool rotateWheels90;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        forward = Input.GetAxis("Vertical");
        turn = Input.GetAxis("Horizontal");
        brake = Input.GetAxis("Jump");

        wheelFL.steerAngle = maxSteerAngle * turn;
        wheelFR.steerAngle = maxSteerAngle * turn;

        // Formula for calculating km/h
        currentSpeed = 2 * 22 / 7 * wheelBL.radius * wheelBL.rpm * 60 / 1000;

        if (currentSpeed < topSpeed)
        {
            // Run the wheels on back left and back right
            wheelBL.motorTorque = maxTorque * forward * motorForce;
            wheelBR.motorTorque = maxTorque * forward * motorForce;
        } // The top speed will not be accurate but will try to slow the car before top steep.

        wheelBL.brakeTorque = maxBrakeTorque * brake;
        wheelBR.brakeTorque = maxBrakeTorque * brake;
        wheelFL.brakeTorque = maxBrakeTorque * brake;
        wheelFR.brakeTorque = maxBrakeTorque * brake;

    }

    private void Update()
    {
        // Rotation of wheel collider
        Quaternion rotation;
        Vector3 position; // Position of wheel collider.

        // Get wheel collider position and rotation.
        wheelFL.GetWorldPose(out position, out rotation);
        fL.transform.position = position;
        if (rotateWheels90)
            rotation *= Quaternion.Euler(0, 90, 0);
        fL.transform.rotation = rotation;

        // Get wheel collider position and rotation.
        wheelFR.GetWorldPose(out position, out rotation);
        fR.transform.position = position;
        if (rotateWheels90)
            rotation *= Quaternion.Euler(0, 90, 0);
        fR.transform.rotation = rotation;

        // Get wheel collider position and rotation.
        wheelBL.GetWorldPose(out position, out rotation);
        bL.transform.position = position;
        if (rotateWheels90)
            rotation *= Quaternion.Euler(0, 90, 0);
        bL.transform.rotation = rotation;

        // Get wheel collider position and rotation.
        wheelBR.GetWorldPose(out position, out rotation);
        bR.transform.position = position;
        if (rotateWheels90)
            rotation *= Quaternion.Euler(0, 90, 0);
        bR.transform.rotation = rotation;

    }

}
