﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiRoll : MonoBehaviour
{
    private Rigidbody rb;

    public WheelCollider WheelFL, WheelFR;
    public WheelCollider WheelBL, WheelBR;
    public float antiRoll = 5000.0f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass += new Vector3(0, 0, 1.0f);
    }

    private void FixedUpdate()
    {
        WheelHit hit;
        float travelL = 1.0f;
        float travelR = 1.0f;

        bool groundedFL = WheelFL.GetGroundHit(out hit);
        if (groundedFL)
            travelL = (-WheelFL.transform.InverseTransformPoint(hit.point).y - WheelFL.radius) / WheelFL.suspensionDistance;

        bool groundedFR = WheelFR.GetGroundHit(out hit);
        if (groundedFR)
            travelR = (-WheelFR.transform.InverseTransformPoint(hit.point).y - WheelFR.radius) / WheelFR.suspensionDistance;

        bool groundedBL = WheelBL.GetGroundHit(out hit);
        if (groundedBL)
            travelL = (-WheelBL.transform.InverseTransformPoint(hit.point).y - WheelBL.radius) / WheelBL.suspensionDistance;

        bool groundedBR = WheelBR.GetGroundHit(out hit);
        if (groundedBR)
            travelR = (-WheelBR.transform.InverseTransformPoint(hit.point).y - WheelBR.radius) / WheelBR.suspensionDistance;

        var antiRollForce = (travelL - travelR) * antiRoll;

        if (groundedFL)
            rb.AddForceAtPosition(WheelFL.transform.up * -antiRollForce,
                   WheelFL.transform.position);
        if (groundedFR)
            rb.AddForceAtPosition(WheelFR.transform.up * antiRollForce,
                   WheelFR.transform.position);
        if (groundedBL)
            rb.AddForceAtPosition(WheelBL.transform.up * -antiRollForce,
                   WheelBL.transform.position);
        if (groundedBR)
            rb.AddForceAtPosition(WheelBR.transform.up * antiRollForce,
                   WheelBR.transform.position);
    }

}
