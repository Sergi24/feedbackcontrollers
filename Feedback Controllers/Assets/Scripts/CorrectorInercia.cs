﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectorInercia : MonoBehaviour
{

    public Rigidbody rb;
    public float smoothness = 0.01f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        // 
        rb.velocity = Vector3.Lerp(rb.velocity, transform.forward * rb.velocity.magnitude, smoothness);
    }

}
