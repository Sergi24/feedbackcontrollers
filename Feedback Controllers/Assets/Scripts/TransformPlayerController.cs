﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformPlayerController : MonoBehaviour
{
    public Camera cam;

    private Vector2 move;

    public float walkSpeed = 2;
    public float runSpeed = 6;
    public float gravity = -12;
    public float jumpHeight = 1.5f;
    [Range(0, 1)]
    public float airControlPercent;

    public float turnSmoothTime = 0.2f;
    private float turnSmoothVelocity;

    public float speedSmoothTime = 0.1f;
    private float speedSmoothVelocity;
    private float currentSpeed;
    private float velocityY;

    private Animator animator;
    private CharacterController cc;

    private void Start()
    {
        animator = GetComponent<Animator>();
        cc = GetComponent<CharacterController>();
    }

    private void Update()
    {
        // Obtenemos el input sin suavizar y lo normalizamos.
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        Vector2 inputDir = input.normalized;
        bool running = Input.GetKey(KeyCode.LeftShift);

        Debug.Log(cc.isGrounded);

        Move(inputDir, running);

        if (Input.GetKeyDown(KeyCode.Space))
            Jump();

        UpdateAnimator(inputDir, running);

    }

    private void Move(Vector2 inputDir, bool running)
    {
        // Si hemos recibido algo miramos si debemos rotar.
        if (inputDir != Vector2.zero)
        {
            float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + cam.transform.eulerAngles.y;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, GetModifiedSmoothTime(turnSmoothTime));
        }

        // Comprobamos si el personaje está corriendo o caminando y añadimos la velocidad pertinente.
        float targetSpeed = ((running) ? runSpeed : walkSpeed) * inputDir.magnitude;
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, GetModifiedSmoothTime(speedSmoothTime));

        velocityY += Time.deltaTime * gravity;
        Vector3 velocity = transform.forward * currentSpeed + Vector3.up * velocityY;

        cc.Move(velocity * Time.deltaTime);
        currentSpeed = new Vector2(cc.velocity.x, cc.velocity.z).magnitude;

        if (cc.isGrounded)
            velocityY = 0;
    }

    private void UpdateAnimator(Vector2 inputDir, bool running)
    {
        // Actualizamos el animator.
        float animationSpeedPercent = ((running) ? currentSpeed / runSpeed : currentSpeed / walkSpeed * .5f) * inputDir.magnitude;
        //animator.SetFloat("speedPercent", animationSpeedPercent, speedSmoothTime, Time.deltaTime);
    }

    private void Jump()
    {
        if (cc.isGrounded)
        {
            float jumpVelocity = Mathf.Sqrt(-2 * gravity * jumpHeight);
            velocityY = jumpVelocity;
        }
    }

    // Permite manejar el control del personaje si está en el aire.
    private float GetModifiedSmoothTime(float smoothTime)
    {
        if (cc.isGrounded)
            return smoothTime;
        if (airControlPercent == 0)
            return float.MaxValue;
        return smoothTime / airControlPercent;
    }

}
