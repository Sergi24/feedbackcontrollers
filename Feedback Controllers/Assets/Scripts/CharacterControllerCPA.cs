﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerCPA : MonoBehaviour
{

    public float velocity = 6.0f;
    public float turn = 90f;
    public float jump = 8.0f;
    public float gravity = 20f;

    private Vector3 movement = Vector3.zero;
    public CharacterController cc;

    private void Update()
    {
        Debug.Log(cc.isGrounded);

        if (cc.isGrounded)
        {
            // Entrada de datos direccional
            float vertical = Input.GetAxis("Vertical");

            // Calculamos vector de avance
            movement = transform.forward;
            movement = movement * vertical * velocity;

            movement = transform.TransformDirection(movement);

            movement *= velocity;



            if (Input.GetButton("Jump"))
            {
                movement.y = jump;
            }

        }
        else
        {
            movement.y -= gravity * Time.deltaTime;
        }

        // Turn
        transform.Rotate(Input.GetAxis("Horizontal") * Vector3.up * turn * Time.deltaTime);

        // Final movement
        cc.Move(movement * Time.deltaTime);


    }



}
