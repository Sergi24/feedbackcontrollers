﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{

    public float mouseSensibility = 10;
    public Transform target;
    public float distanceTarget = 2;
    public Vector2 pitchMinMax = new Vector2(-40, 85);

    public float rotationSmoothTime = .12f;
    Vector3 rotationSmoothVelocity;
    Vector3 currentRotation;

    float yaw;
    float pitch;

    private void LateUpdate()
    {
        yaw += Input.GetAxis("Mouse X") * mouseSensibility;
        pitch -= Input.GetAxis("Mouse Y") * mouseSensibility;
        pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);

        currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);

        transform.eulerAngles = currentRotation;


        transform.position = target.position + target.GetComponent<CharacterController>().center - transform.forward * distanceTarget;
        Debug.Log(target.GetComponent<CharacterController>().center);
        //GetComponent<CharacterController>().center
    }

}
