﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLerp : MonoBehaviour
{
    public Transform target;

    public bool followPosition, followRotation;
    public float positionDistance, rotationDistance;


    // Update is called once per frame
    void Update()
    {
        if (followPosition)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, positionDistance);
        }

        if (followRotation)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, rotationDistance);
        }
    }
}
