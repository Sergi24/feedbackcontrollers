﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour
{

    public Transform playerTransform;
    public Transform pivotTransform;

    private Vector3 cameraOffSet;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    public bool lookAtPlayer;

    private void Start()
    {
        cameraOffSet = transform.position - playerTransform.position;
    }

    private void Update()
    {
        Vector3 newPos = playerTransform.position + cameraOffSet;

        transform.position = Vector3.Slerp(transform.position, pivotTransform.position, SmoothFactor);

        if (lookAtPlayer)
            transform.LookAt(playerTransform);
    }

}
