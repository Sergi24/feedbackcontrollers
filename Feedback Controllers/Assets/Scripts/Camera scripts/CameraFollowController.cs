﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowController : MonoBehaviour
{

    public Transform objectToFollow;
    public Vector3 offSet;
    public Vector3 rotationOffSet;
    public float followSpeed = 10;
    public float lookSpeed = 10;

    public void LookAtTarget()
    {
        Vector3 extraRotation = new Vector3(rotationOffSet.z, rotationOffSet.x, rotationOffSet.y);


        Vector3 lookDirection = (objectToFollow.position) - transform.position;
        Quaternion rot = Quaternion.LookRotation(lookDirection, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, lookSpeed * Time.deltaTime);
    }

    public void MoveToTarget()
    {
        Vector3 targetPos = objectToFollow.position +
                             objectToFollow.forward * offSet.z +
                             objectToFollow.right * offSet.x +
                             objectToFollow.up * offSet.y;
        transform.position = Vector3.Lerp(transform.position, targetPos, followSpeed * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        LookAtTarget();
        MoveToTarget();
    }
}
