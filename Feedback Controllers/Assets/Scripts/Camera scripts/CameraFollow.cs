﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target;

    public float smoothPositionSpeed = 0.125f;
    public float smoothRotationSpeed = 0.125f;
    public Vector3 offset;

    private void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothPositionSpeed);
        transform.position = smoothedPosition;

        Quaternion desiredRotation = Quaternion.Euler(target.position + offset);
        Quaternion smoothedRotation = Quaternion.Lerp(transform.rotation, desiredRotation, smoothPositionSpeed);
        transform.rotation = smoothedRotation;


    }

}
