﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMainCamera : MonoBehaviour
{

    private Camera cam;
    public bool rotate180 = false;

    // Update is called once per frame
    void Update()
    {
        cam = Camera.main;

        if (cam != null)
        {
            transform.LookAt(cam.transform);
            if (rotate180)
                transform.rotation *= Quaternion.Euler(0, 180, 0);
        }
        else
            if (Camera.main != null)
            cam = Camera.main;
    }
}
