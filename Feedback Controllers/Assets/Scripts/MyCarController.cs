﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyCarController : MonoBehaviour
{
    public List<WheelInfo> wheels;
    public float maxMotorTorque;
    public float maxSteeringAngle;

    [Range(0, 1)]
    [SerializeField]
    private float steerHelper; // 0 is raw physics , 1 the car will grip in the direction it is facing

    public float currentSpeed;
    public float maxSpeed = 120;

    [Range(0, 1)]
    [SerializeField]
    public float decelerationSpeed;
    private float maxHandBrakeTorque;

    private Rigidbody rb;
    private float oldRotation;

    public bool rotateWheels90Degrees;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        maxHandBrakeTorque = float.MaxValue;
    }

    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        // Si no tiene hijo sale (es donde está la rueda visual)
        if (collider.transform.childCount == 0)
        {
            return;
        }
        // Obtenemos el modelo de la rueda.
        Transform visualWheel = collider.transform.GetChild(0);

        // Obtenemos la worldpose de la rueda en cuestión
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        // En este modelo que he cogido las ruedas están rotadas -90 grados, lo arreglo con este booleano.
        if (rotateWheels90Degrees)
            rotation *= Quaternion.Euler(0, 90, 0);

        // Actualizamos la transform del modelo.
        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    public void FixedUpdate()
    {
        // Obtenemos el input de los valores necesarios.
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
        float handbrake = Input.GetAxis("Jump");

        // Nos aseguramos que handbrake está entre 0 y 1
        handbrake = Mathf.Clamp(handbrake, 0, 1);

        // Por cada rueda comprobamos...
        foreach (WheelInfo wheelInfo in wheels)
        {
            // Es una rueda que varia la dirección del coche? Entonces le aplicamos el cambio de dirección...
            if (wheelInfo.steering)
            {
                wheelInfo.wheel.steerAngle = steering;
            }

            // Es una rueda que produce fuerza para que se mueva el coche? Entonces le pones fuerza al valor del motor.
            if (wheelInfo.motor)
            {
                if (motor != 0)
                    // Si supera la velocidad límite no agregamos fuerza de motor.
                    if (currentSpeed >= maxSpeed)
                    {
                        wheelInfo.wheel.motorTorque = 0;
                    }
                    else
                        wheelInfo.wheel.motorTorque = motor;
                else
                {
                    wheelInfo.wheel.motorTorque = 0;
                }
            }

            // Si no está acelerando desacelera.
            if (motor == 0)
                wheelInfo.wheel.brakeTorque = decelerationSpeed * maxHandBrakeTorque;

            // Si no está acelerando y va a una velocidad muy baja se para.
            if (motor == 0 && currentSpeed < 2f && currentSpeed > -2f)
            {
                rb.velocity = Vector3.zero;
                wheelInfo.wheel.brakeTorque = Mathf.Infinity;
            }

            // Si el jugador está frenando, frenamos
            if (handbrake > 0f)
                wheelInfo.wheel.brakeTorque = handbrake * maxHandBrakeTorque;
            else // Si no está frenando reducimos el frenado a 0.
                wheelInfo.wheel.brakeTorque = 0;

            // Actualizamos cada rueda visual con el transform correspondiente.
            ApplyLocalPositionToVisuals(wheelInfo.wheel);

        }

        // Una ayudita para girar más cómodamente.
        SteerHelper();
        // Obtenemos la velocidad en km/h
        currentSpeed = rb.velocity.magnitude * 3.6f;

    }

    private void SteerHelper()
    {
        // Verificamos que todas las ruedas están tocando el suelo. En caso de no ser así anulamos la ayuda de giro.
        foreach (WheelInfo wheelInfo in wheels)
        {
            WheelHit wheelhit;
            wheelInfo.wheel.GetGroundHit(out wheelhit);
            if (wheelhit.normal == Vector3.zero)
                return;
        }

        // Miramos si el giro no pasa del límite y de ser así aplicamos la rotación pertinente.
        if (Mathf.Abs(oldRotation - transform.eulerAngles.y) < 10f)
        {
            float turnAdjust = (transform.eulerAngles.y - oldRotation) * steerHelper;
            Quaternion velRotation = Quaternion.AngleAxis(turnAdjust, Vector3.up);
            rb.velocity = velRotation * rb.velocity;
        }
        // Guardamos el angulo Y para el siguiente frame verificar si necesitamos o no la ayuda.
        oldRotation = transform.eulerAngles.y;
    }

}

[System.Serializable]
public class WheelInfo
{
    public WheelCollider wheel;

    public bool motor;
    public bool steering;
}