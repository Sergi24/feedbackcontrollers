﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public Canvas mainCanvas;
    public List<GameObject> allControllers;
    public bool paused = true;

    private void Start()
    {
        Time.timeScale = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    public void ActiveController(int i)
    {
        foreach (GameObject gO in allControllers)
        {
            gO.SetActive(false);
        }

        allControllers[i].SetActive(true);
        Pause();

    }

    private void Pause()
    {
        paused = !paused;
        if (Time.timeScale == 1)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;

        Debug.Log(mainCanvas.gameObject.activeSelf);
        mainCanvas.gameObject.SetActive(!mainCanvas.gameObject.activeSelf);
    }

    public void ReturnMainMenu()
    {
        SceneManager.LoadScene("Main");
    }

}
